# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "17/07/2017"


from orangecontrib.silx.widgets.Plot1D import Plot1D
from silx.gui.test.utils import TestCaseQt
import unittest
from orangecontrib.silx.utils import QApplicationManager

_qapp = QApplicationManager.getInstance()

class TestPlot1D(TestCaseQt):
    """Simple test on the Plot1d class
    """

    @classmethod
    def setUpClass(cls):
        TestCaseQt.setUpClass()
        cls.x = (0, 1, 2, 3, 5)
        cls.y = (5, 4, 5, 6, 1)
        cls.plotWidget = Plot1D()

    def testInputs(self):
        self.plotWidget.addCurve({'x': self.x, 'y': self.y, 'legend': 'test'})


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestPlot1D, ):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")