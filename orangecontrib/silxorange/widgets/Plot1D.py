# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "17/07/2017"

import AnyQt.QtWidgets as QWS
import AnyQt.QtCore as QtCore
from Orange.widgets.widget import OWWidget
from silx.gui.plot.PlotWindow import PlotWindow






class TabBar(QWS.QTabBar):
    def __init__(self, parent=None, editingCallBack = None):
        QWS.QTabBar.__init__(self, parent)
        self._editor = QWS.QLineEdit(self)
        self._editor.setWindowFlags(QtCore.Qt.Popup)
        self._editor.setFocusProxy(self)
        self._editor.editingFinished.connect(self.handleEditingFinished)
        self._editor.installEventFilter(self)
        self.editingCallBack = editingCallBack

    def eventFilter(self, widget, event):
        if ((event.type() == QtCore.QEvent.MouseButtonPress and
             not self._editor.geometry().contains(event.globalPos())) or
            (event.type() == QtCore.QEvent.KeyPress and
             event.key() == QtCore.Qt.Key_Escape)):
            self._editor.hide()
            return True
        return QWS.QTabBar.eventFilter(self, widget, event)

    def mouseDoubleClickEvent(self, event):
        index = self.tabAt(event.pos())
        if index >= 0:
            self.editTab(index)

    def editTab(self, index):
        rect = self.tabRect(index)
        self._editor.setFixedSize(rect.size())
        self._editor.move(self.parent().mapToGlobal(rect.topLeft()))
        self._editor.setText(self.tabText(index))
        if not self._editor.isVisible():
            self._editor.show()

    def handleEditingFinished(self):
        index = self.currentIndex()
        if index >= 0:
            oldtext = self.tabText(index)
            self._editor.hide()
            self.setTabText(index, self._editor.text())
            newtext = self.tabText(index)
            if self.editingCallBack is not None:
                self.editingCallBack( index,  oldtext, newtext    ) 
            
            
class Plot1D(OWWidget):
    """
    Simple class to show content of a numpy array throw different views
    """
    name = "silx plot 1D"
    # icon = "icons/plot-widget.svg"
    icon = "icons/viewspectra.png"
    want_main_area = True

    priority = 9

    category = "silx"
    keywords = ["plot", "fit", "data", "1D"]

    inputs = [("data", dict, "addCurve")]
    outputs = []

    def __init__(self):
        super().__init__()

        ## self._plot = PlotWindow()
        ##  self._plot.getFitAction().setVisible(True)
        ##self.controlArea.layout().addWidget(self._plot)
        self.qtbw = QWS.QTabWidget()
        self.qtbw.setTabBar( TabBar(editingCallBack = self.editingCallBack) ) 
        self.controlArea.layout().addWidget(self.qtbw)
        self.myplots={}


    def addCurve(self, data):
        if data is None:
            return 
        if isinstance(data,list):
            for tok in data:
                self.addCurveMono(tok)
        else:
            self.addCurveMono(data)


    def editingCallBack( self, index,  oldtext, newtext    ) :
        
        if newtext == "":
            del self.myplots[ oldtext ]
            self.qtbw.removeTab(index)
            return
        
        if oldtext != newtext:
            self.myplots[ newtext ] = self.myplots[ oldtext ]
            del self.myplots[ oldtext ]
        

    def addCurveMono(self, data):
        """
        add a curve from the data dictionnary

        :param dict data: should contain 'x' and 'y' keys 
        """
        print("ADD CURVE")
        assert 'title' in data


        if data["title"] not in self.myplots:
            self.myplots[ data["title"]  ] =  PlotWindow()
            self.myplots[ data["title"]  ].getFitAction().setVisible(True)
            self.qtbw.addTab(  self.myplots[ data["title"]  ] , data["title"] )
            
        
        assert 'x' in data
        assert 'y' in data
        assert 'legend' in data
        replace=True
        if "replace" in data:
            replace = data["replace"]

        print("LEGEND ", data["legend"])
        print("replace ", replace)
        self.myplots[data["title"] ].addCurve(x=data['x'], y=data['y'], legend=data["legend"], replace=replace)

    def getPlot(self):
        """
        
        :return: the silx Plot object 
        """
        return self._plot


def main():
    from silx.gui import qt
    x = (0, 1, 2, 3, 5)
    y = (5, 4, 5, 6, 1)
    legend = 'testCurve'

    app = qt.QApplication([])
    s = Plot1D()
    s.addCurve({'x': x, 'y': y, 'legend': legend})
    s.show()
    app.exec_()


if __name__ == "__main__":
    main()
