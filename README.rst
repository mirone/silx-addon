Orange3 ESRF Add-on
======================

This project contains widgets made for the esrf usage in order to define workflows, using the Orange3 canvas and core(http://orange.biolab.si).

Installation
------------

Step 0 - Create a virtual env
'''''''''''''''''''''''''''''

It is recommended to create a python virtual environment to run the workflow tool.
Virtual environment might avoid some conflict between python packages. But you can also install it on your 'current' python environment and move to step 1.

.. code-block:: bash

   virtualenv --python=python3 --system-site-packages myvirtualenv


Then activate the virtual environment

.. code-block:: bash

   source myvirtualenv/bin/activate

.. note:: To quit the virtual environment

   .. code-block:: bash

      deactivate

Step 1 - Orange3 installation
'''''''''''''''''''''''''''''

You will need a fork of the original Orange project in order to run the silx-addon project.
This is needed because small modification have been made in order to get the behavio we wanted (has looping workflows).

The fork is accessible here : https://github.com/payno/orange3.git

So install this fork :

.. code-block:: bash

   git clone https://github.com/payno/orange3.git
   cd orange3
   pip install -r requirements.txt
   pip install -r requirements-gui.txt
   pip install .

.. note:: if you have an old PyQt version, you might need to have a look at https://pythonhosted.org/silx/virtualenv.html?highlight=virtualenv to avoid rebuild of sip... you might want to create a symbolic link:

   If you want to use PyQt4 installed in */usr/lib/python2.7/dist-packages/*:

   .. code-block:: bash

      ln -s /usr/lib/python2.7/dist-packages/PyQt4 silx_venv/lib/python2.7/site-packages/
      ln -s /usr/lib/python2.7/dist-packages/sip.so silx_venv/lib/python2.7/site-packages/


Step 2 - silx-addon
'''''''''''''''''''

clone the silx-addon project

.. code-block:: bash

   git clone git@gitlab.esrf.fr:silx/silx-addon.git


then install it

.. code-block:: bash

   cd silx-addon
   pip install -r requirements.txt
   pip install -e .


.. note:: -e option will register the add-on into Orange, but you shouldn't copy it into the Python's site-packages directory. This is due to the Orange add-on installation procedure. That mean also that any modification into this source code will be apply during execution time.


Make sure the installation whent well, and that Orange is running correctly.

.. code-block:: bash

   python run_tests.py

Unit test should be executed without any error.


Launching Orange
::::::::::::::::

you can simply execute the command:

.. code-block:: bash

   orange-canvas


.. note:: if your installed a virtual environment do not forget to active it :

.. code-block:: bash

   source myvirtualenv/bin/activate


Documentation
:::::::::::::

.. code-block:: bash

   cd doc
   make html

The documentation is build in doc/build/html and the entry point is index.html

.. code-block:: bash

   firefox build/html/index.html

.. note:: the build of the documentation need sphinx to be installed. This is not an hard dependacy. So you might need to install it.


You also should generate documentation to be accessible from Orange GUI (pressing the F1 key).

.. code-block:: bash

   cd doc
   make htmlhelp